# Quadtree 

## Introducción 

 [Quadtree](https://en.wikipedia.org/wiki/Quadtree) es un tipo de estructura de datos que se utiliza para poder realizar busquedas eficientes de pocos elementos sobre espacios de N dimensiones, donde existen muchos elementos.

Este tipo de estructuras son muy utilizadas dentro de aplicaciones `gis` donde se intenta buscar elementos ya sea por distancia, detro de en un area, etc. De modo que tenga tiempos de respuesta bajos en consultas de miles de puntos. 

## Estructura

La representación tipica de la estructura de un `quadtree` es en forma de arbol donde el espacio se va subdiviendiendo en regiones regulares que cumplen un limite de densidad, esto implica que la profundidad de una hoja no es conocida hasta que no se construye el la estructura

La mejor simplificación para implementar tanto la construcción como los métodos de busqueda dentro de un `quadtree`, es mediante la recursión. En este ejercicio solo se permitirá el uso de la recursividad para resolver el problema. 

Puedes encontrar información del `quadtree` en el siguiente [enlace](https://en.wikipedia.org/wiki/Quadtree), 

## Implementación

Este ejercicio trata de implementar una clase en java a partir de la *interface* que se añade dentro del directorio `src`, por lo que hay que ajustarse **al menos** a los métodos que se detallan en dicha *interface*. 

Se valorará positivamente la eficiencia, simplicidad, y el diseño. Así como aportar nuevos métodos que cubran futuras necesidades para el `quadtree`.




