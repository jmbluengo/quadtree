package com.lda.bigdata.college;

/**
 * Implementacion de la interfaz de Indece en forma de arbol acotado.
 */
public abstract class BoundedTree implements TreeIndex {
    final int maxLeafSize;
    final double xMin;
    final double yMin;
    final double xMax;
    final double yMax;

    /**
     * Inicializa el indice de arbol acotado.
     *
     * @param maxLeafSize Capacidad de numero de puntos que caben en cada hoja del arbol.
     * @param xMin        Minima coordenada x permitida.
     * @param yMin        Minima coordenada y permitida.
     * @param xMax        Maxima coordenada x permitida.
     * @param yMax        Maxima coordenada y permitida.
     */
    protected BoundedTree(int maxLeafSize, double xMin, double yMin, double xMax, double yMax) {
        this.maxLeafSize = maxLeafSize;
        this.xMin = xMin;
        this.yMin = yMin;
        this.xMax = xMax;
        this.yMax = yMax;
    }
}
