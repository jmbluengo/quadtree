package com.lda.bigdata.college;

import org.locationtech.jts.geom.Coordinate;

import java.util.HashSet;

/**
 * Implementacion de un indice Quadtree.
 *
 * @see <a href="https://en.wikipedia.org/wiki/Quadtree</a>
 */
@SuppressWarnings("unused")
public class QuadTree extends BoundedTree {

    /**
     * Inicializa el indice de arbol acotado.
     *
     * @param maxLeafSize Capacidad de numero de puntos que caben en cada hoja del arbol.
     * @param xMin        Minima coordenada x permitida.
     * @param yMin        Minima coordenada y permitida.
     * @param xMax        Maxima coordenada x permitida.
     * @param yMax        Maxima coordenada y permitida.
     */
    protected QuadTree(int maxLeafSize, double xMin, double yMin, double xMax, double yMax) {
        super(maxLeafSize, xMin, yMin, xMax, yMax);
    }

    /**
     * Inserta un nuevo punto en el indice.
     *
     * @param p Punto a insertar.
     * @return Verdadero si el punto se ha insertado correctamente; Falso en caso contrario.
     */
    public boolean insert(IndexedPoint p) {
        return false;
    }

    /**
     * Devuelve una lista de los puntos que se encuentran a una distancia dada del punto consulta.
     *
     * @param queryPoint Punto consulta, o centro de la consulta.
     * @param range      Distancia maxima permitida para encontrar puntos cercanos.
     * @return Lista de los puntos que se encuentra a una distancia igual o menor del punto de
     * consulta.
     */
    public HashSet<IndexedPoint> rangeQuery(Coordinate queryPoint, double range) {
        return null;
    }

    /**
     * Devuelve una lista de los {@code n} puntos que se encuentran a una distancia dada del punto
     * consulta.
     *
     * @param queryPoint Punto consulta, o centro de la consulta.
     * @param range      Distancia maxima permitida para encontrar puntos cercanos.
     * @param n          Numero maximo de puntos a devolver.
     * @return Lista de los {@code n} puntos que se encuentra a una distancia igual o menor del punto de
     * consulta.
     */
    public HashSet<IndexedPoint> knnQuery(Coordinate queryPoint, double range, int n) {
        return null;
    }

    /**
     * Devuelve una lista con todos los puntos indexados.
     *
     * @return Lista de todos los puntos indexados.
     */
    public HashSet<IndexedPoint> queryAll() {
        return null;
    }

    /**
     * Devuelve el numero de puntos contenidos en el indice.
     *
     * @return Numero de puntos contenidos en el indice.
     */
    public int size() {
        return 0;
    }

    /**
     * Devuelve la profundidad del arbol.
     *
     * @return Profundidad del arbol.
     */
    public int depth() {
        return 0;
    }
}
