package com.lda.bigdata.college;

import org.locationtech.jts.geom.Coordinate;

import org.apache.commons.codec.binary.Hex;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Stream;

/**
 * Clase que representa un punto en el espacio (2d) con un identificador.
 */
@SuppressWarnings("ALL")
public class IndexedPoint {
    private final String id;
    private final Coordinate point;
    private static MessageDigest md;

    static {
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Inicializa un punto indexado con un id en formato entero y un punto de la libreria JTS.
     *
     * @param id    Identificador unico del punto.
     * @param point Punto en el espacio 2D.
     */
    public IndexedPoint(String id, Coordinate point) {
        this.id = id;
        this.point = point;
    }

    public String getId() {
        return id;
    }

    public Coordinate getPoint() {
        return point;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexedPoint that = (IndexedPoint) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", IndexedPoint.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("point=" + point)
                .toString();
    }

    /**
     * Devuelve un stream de puntos aleatorios contenidos dentro de una extension.
     *
     * @param seed Semilla para la creacion de numeros aleatorios.
     * @param xMin Minima coordenada x permitida para los puntos aleatorios.
     * @param xMax Maxima coordenada x permitida para los puntos aleatorios.
     * @param yMin Minima coordenada y permitida para los puntos aleatorios.
     * @param yMax Maxima coordenada y permitida para los puntos aleatorios.
     * @return Un punto aleatorio contenido en la extension dada.
     */
    public static Stream<IndexedPoint> indexedPointStream(long seed, double xMin, double xMax, double yMin, double yMax) {
        Random rgn = new Random(seed);
        PrimitiveIterator.OfDouble xIterator = rgn.doubles(xMin, xMax).iterator();
        PrimitiveIterator.OfDouble yIterator = rgn.doubles(yMin, yMax).iterator();

        new Coordinate(1, 2);
        return rgn.ints().mapToObj(
                id -> {
                    Coordinate c = new Coordinate(xIterator.next(), yIterator.next());
                    String idString = IndexedPoint.createId(id,c);
                    return new IndexedPoint(idString, c);
                }
        );
    }

    /**
     * Devuelve un stream de puntos aleatorios contenidos dentro de una extension delimitada
     * por un radio y un punto central.
     *
     * @param seed   Semilla para la creacion de numeros aleatorios.
     * @param radius Minima coordenada x permitida para los puntos aleatorios.
     * @return Un punto aleatorio contenido en la extension dada por el radio.
     */
    public static Stream<IndexedPoint> indexedPointStream(long seed, double radius, Coordinate center) {
        Random rgn = new Random(seed);
        PrimitiveIterator.OfDouble doubleIterator = rgn.doubles(-radius, radius).iterator();

        new Coordinate(1, 2);
        return rgn.ints().mapToObj(
                id -> {
                    Coordinate c = new Coordinate(
                            center.x+doubleIterator.nextDouble(),
                            center.y+doubleIterator.nextDouble()
                    );
                    String idString = IndexedPoint.createId(id,c);
                    return new IndexedPoint(idString, c);
                }
        );
    }

    /**
     * Devuelve un string identificativo del punto dado un entero y un punto.
     *
     * @param integer Entero para diferenciar puntos con las mismas coordenadas.
     * @param point   Coordenadas del punto a identificar.
     * @return String identificador del punto.
     */
    public static String createId(int integer, Coordinate point) {
        String toDigest = String.valueOf(integer) + point.x + point.y;
        return Hex.encodeHexString(md.digest(toDigest.getBytes(StandardCharsets.UTF_8))).toUpperCase(Locale.ROOT);
    }
}
