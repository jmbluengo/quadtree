package com.lda.bigdata.college;


import org.locationtech.jts.geom.Coordinate;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Interfaz de un indice en forma de arbol.
 */
@SuppressWarnings("unused")
public interface TreeIndex extends Serializable {

    /**
     * Inserta un nuevo punto en el indice. Si el punto ya esta contenido en el indice no se inserta.
     *
     * @param p Punto a insertar.
     * @return Verdadero si el punto se ha insertado correctamente; Falso en caso contrario.
     */
    boolean insert(IndexedPoint p);

    /**
     * Devuelve una lista de los puntos que se encuentran a una distancia dada del punto consulta.
     *
     * @param queryPoint Punto consulta, o centro de la consulta.
     * @param range      Distancia maxima permitida para encontrar puntos cercanos.
     * @return Lista de los puntos que se encuentra a una distancia igual o menor del punto de
     * consulta.
     */
    HashSet<IndexedPoint> rangeQuery(Coordinate queryPoint, double range);

    /**
     * Devuelve una lista de los {@code n} puntos que se encuentran a una distancia dada del punto
     * consulta.
     *
     * @param queryPoint Punto consulta, o centro de la consulta.
     * @param range      Distancia maxima permitida para encontrar puntos cercanos.
     * @param n          Numero maximo de puntos a devolver.
     * @return Lista de los {@code n} puntos que se encuentra a una distancia igual o menor del punto de
     * consulta.
     */
    HashSet<IndexedPoint> knnQuery(Coordinate queryPoint, double range, int n);

    /**
     * Devuelve una lista con todos los puntos indexados.
     *
     * @return Lista de todos los puntos indexados.
     */
    HashSet<IndexedPoint> queryAll();

    /**
     * Devuelve el numero de puntos contenidos en el indice.
     *
     * @return Numero de puntos contenidos en el indice.
     */
    int size();

    /**
     * Devuelve la profundidad del arbol.
     *
     * @return Profundidad del arbol.
     */
    int depth();
}
