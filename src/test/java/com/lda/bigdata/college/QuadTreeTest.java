package com.lda.bigdata.college;


import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.locationtech.jts.geom.Coordinate;


public class QuadTreeTest {

    @Test
    public void insert() {

        // Definimos los rangos de valores para las coordenadas de los puntos.
        double xMin = -5;
        double xMax = 5;
        double yMin = -5;
        double yMax = 5;

        // Creamos puntos aleatorios.
        Set<IndexedPoint> points = IndexedPoint.indexedPointStream(
                0L, xMin, xMax, yMin, yMax
        ).limit(100).collect(Collectors.toSet());

        // Instanciamos un QuadTree
        TreeIndex quadTree = new QuadTree(10, xMin, yMin, xMax, yMax);

        // Insertamos los puntos
        int totalPointsInserted = points.stream().map(quadTree::insert).mapToInt(output -> (output) ? 1 : 0).sum();

        // Comprobamos el numero de puntos indexados.
        Assert.assertEquals(
                points.size(), totalPointsInserted
        );

        // Comprobamos que se han indexado todos los puntos correctamente.
        Assert.assertEquals(
                points, quadTree.queryAll()
        );
    }

    @Test
    public void rangeQuery() {

        /*
            Definimos los puntos que estaran fuera de la consulta.
         */
        Set<IndexedPoint> pointsOutside = IndexedPoint.indexedPointStream(
                0L, 0, 5, 0, 5
        ).limit(100).collect(Collectors.toSet());

        /*
            Definimos los puntos que estaran fuera dentro de la consulta.
         */
        Coordinate center = new Coordinate(-2.5, -2.5);
        double radius = 2.0;
        Set<IndexedPoint> pointsInside = IndexedPoint.indexedPointStream(
                0L, radius, center
        ).limit(20).collect(Collectors.toSet());

        // Unimos los sets de puntos.
        HashSet<IndexedPoint> allPoints = new HashSet<>();
        allPoints.addAll(pointsInside);
        allPoints.addAll(pointsOutside);

        // Instanciamos un QuadTree
        TreeIndex quadTree = new QuadTree(10, -5, -5, 5, 5);

        // Insertamos todos los puntos
        allPoints.forEach(quadTree::insert);

        // Obtenemos el resultado de la consulta.
        HashSet<IndexedPoint> resultPoints = quadTree.rangeQuery(center, radius);

        // Comprobamos que los puntos son los mismos.
        Assert.assertEquals(pointsInside, resultPoints);
    }

    @Test
    public void knnQuery() {
        /*
            Definimos los puntos que estaran fuera de la consulta.
         */
        Set<IndexedPoint> pointsOutside = IndexedPoint.indexedPointStream(
                0L, 0, 5, 0, 5
        ).limit(100).collect(Collectors.toSet());

        /*
            Definimos los puntos que estaran fuera dentro de la consulta.
         */
        Coordinate center = new Coordinate(-2.5, -2.5);
        double radius = 2.0;
        Set<IndexedPoint> pointsInside = IndexedPoint.indexedPointStream(
                0L, radius, center
        ).limit(20).collect(Collectors.toSet());

        // Unimos los sets de puntos.
        HashSet<IndexedPoint> allPoints = new HashSet<>();
        allPoints.addAll(pointsInside);
        allPoints.addAll(pointsOutside);

        // Instanciamos un QuadTree
        TreeIndex quadTree = new QuadTree(10, -5, -5, 5, 5);

        // Insertamos todos los puntos
        allPoints.forEach(quadTree::insert);

        // Obtenemos el resultado de la consulta.
        HashSet<IndexedPoint> resultPoints = quadTree.knnQuery(center, radius,pointsInside.size());

        // Comprobamos que los puntos son los mismos.
        Assert.assertEquals(pointsInside, resultPoints);
    }

    @Test
    public void depth() {

        /*
            Definimos los puntos solo en un cuadrante.
         */
        Coordinate center = new Coordinate(-2.5, -2.5);
        double radius = 2.0;
        Set<IndexedPoint> points = IndexedPoint.indexedPointStream(
                0L, radius, center
        ).limit(20).collect(Collectors.toSet());


        /*
            Instanciamos un QuadTree con tamano de hoja maximo unidad para que cada punto insertado
            subdivida el arbol.
         */
        TreeIndex quadTree = new QuadTree(1, -5, -5, 5, 5);

        // Insertamos todos los puntos
        points.forEach(quadTree::insert);

        // Comprobamos que la profundidad del arbol es igual al numero de puntos indexados.
        Assert.assertEquals(
                points.size(), quadTree.depth()
        );
    }
}